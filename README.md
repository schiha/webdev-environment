# webdev-environment

easy web development environment with Gulp4 inluding JavaScript ES6 compiling, SCSS and minified files.

#### build & run

- clone repository
- cd into repository
- run `npm install`
- open terminal window
- run `gulp build` to create dist files
- run `gulp` to start gulp watcher

#### Happy Coding!