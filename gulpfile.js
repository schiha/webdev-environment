// gulp dependencies
const { src, dest, task, watch, series, parallel, env } = require('gulp');

// html related plugins
var htmlmin         = require('gulp-html-minifier');

// css related plugins
var sass            = require('gulp-sass'),
    autoprefixer    = require('gulp-autoprefixer');

// js related plugins
var babel           = require('gulp-babel'),
    uglify          = require('gulp-uglify'),
    browserify      = require('gulp-browserify');

// browser related plugins
var browserSync     = require('browser-sync');

// utility plugins
var rename          = require('gulp-rename'),
    sourcemaps      = require('gulp-sourcemaps'),
    plumber         = require('gulp-plumber'),
    concat          = require('gulp-concat'),
    clean           = require('gulp-clean');

// source folders
var src_folder      = './src/',
    src_folder_css  = './src/assets/sass/',
    src_folder_js   = './src/assets/js/',
    dist_folder_css = './dist/assets/css/',
    dist_folder_js  = './dist/assets/js/',
    dist_folder     = './dist/';

// browser sync
function browser_sync(callback) {
    browserSync.init({
        server: {baseDir: dist_folder},
        browser: ["google chrome"]
    });
    callback();
}

// minify html
function html(callback){
    src(dist_folder + '*.html', {force: true})
        .pipe(clean());
    src(src_folder + '*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(dest(dist_folder))
        .pipe(browserSync.stream());
    callback();
}

// minify style
function style(callback) {
    src(dist_folder_css + '*.*', {force: true})
        .pipe(clean());
    src(src_folder_css + '*.sass')
        .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(rename({ basename: 'style' }))
            .pipe(sass({
                errLogToConsole: true,
                outputStyle: 'compressed'
            }))
            .pipe(autoprefixer())
            .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(dist_folder_css))
        .pipe(browserSync.stream());
    callback();
}

// minify javascript
function js(callback) {
    src(dist_folder_js + '*.*', {force: true})
        .pipe(clean());
    src(src_folder_js + '*.js')
        .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(concat('main.js'))
            .pipe(babel({presets: ['@babel/env']}))
            .pipe(browserify({
                insertGlobals: true,
                debug: !env
            }))
            .pipe(uglify())
            .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(dist_folder_js))
        .pipe(browserSync.stream());
    callback();
}

// watcher
function fileWatcher(){
    watch(src_folder + '*.html', series(html));
    watch(src_folder_css + '*.sass', series(style));
    watch(src_folder_js + '*.js', series(js));
}

// tasks
task('html', html);
task('style', style);
task('js', js);

task('build', parallel(html, style, js));
task('default', parallel(browser_sync, fileWatcher));